const gulp       = require('gulp');
const include    = require('gulp-file-include');
const typescript = require('gulp-typescript');


const folder = {
  include : {
    html : 'src/app/html/*.html',
    css  : 'src/app/css/*.css',
    js   : 'src/app/js/*.js',
    svg  : 'src/app/img/*.svg',
    dest : 'build/app/'
  },
  tsApp  : {
    src  : 'src/app/ts/*.ts',
    dest : 'src/app/js/'
  },
  tsNode : {
    src  : 'src/*.ts',
    dest : 'build/'
  }
}

gulp.task('default', () => {
  gulp.watch(folder.include.html, ['include']);
  gulp.watch(folder.include.css,  ['include']);
  gulp.watch(folder.include.js,   ['include']);
  gulp.watch(folder.tsApp.src,    ['compile1']);
  gulp.watch(folder.tsNode.src,   ['compile2']);
});

gulp.task('include', () => {
  return gulp.src([folder.include.html])
  .pipe(include({
    prefix   : '@@',
    basepath : '@file'
  }))
  .pipe(gulp.dest(folder.include.dest));
});

gulp.task('compile1', () => {
  return gulp.src(folder.tsApp.src)
    .pipe(typescript())
    .pipe(gulp.dest(folder.tsApp.dest))
})
gulp.task('compile2', () => {
  return gulp.src(folder.tsNode.src)
    .pipe(typescript())
    .pipe(gulp.dest(folder.tsNode.dest))
})
