const http  = require('http').createServer(handler);  // WebServer
const io    = require('socket.io')(http);             // Communication with WebServer
const fs    = require('fs');                          // Write/Read all files
const color = require('colors');

// Our utility
const util = require('./util');

// Set Theme color
color.setTheme({
  ok  : 'green',
  log : 'yellow',
  err : ['red', 'blod']
});

//---------------------------------------------------------------------------//
//-------------------------------- WEB SIDE ---------------------------------//
//---------------------------------------------------------------------------//

// Listen on 127.0.0.1:8127
http.listen(8127, '127.0.0.1');

// Handler for webserver
function handler(req, res) {
  // If '.' then redirect to the main page
  let reqFilePath = '.' + (req.url === '/'? '/index.html': req.url);

  fs.readFile(`app/${reqFilePath}`, (error, data) => {
    if (error) {
      res.writeHead(500);
      res.end(`Error loading '${reqFilePath}'`);
      console.error(error);
      return;
    }

    res.writeHead(200, {'Content-Type' : util.getMimeType(reqFilePath)});
    res.end(data);
  });
}

// Interface/Prototopy of object  of objects
interface State {
  accelerator   : number;
  brake         : number;
  drs           : boolean;
  rollBar       : boolean;
  glv           : boolean;
  onChange     ?: any;
  triggerChange?: any;
}

interface StateChangeHandler {
  (state: State, channel: string): void;
}

// Pin state and upgrade
var state: State = {
  accelerator  : 0,
  brake        : 0,
  drs          : false,
  rollBar      : false,
  glv          : false,
  // Bind a listener to the Change Event
  onChange     : (handler: StateChangeHandler, namespace: string) => {
    state.onChange.handlers.push(handler);
  },
  // Trigger the Change Event. Call it when you change the state
  triggerChange: () => {
    let channel: string = 'state'; // default channel

    let toCheck: string[] = [
      'accelerator',
      'brake',
      'drs',
      'rollBar',
      'glv'
    ];
    toCheck.forEach((prop) => {
      if (state[prop] !== state.triggerChange.oldState[prop]) {
        console.log(`${prop} --> ${state[prop]}`);
        // Update
        state.triggerChange.oldState[prop] = state[prop];

        if (prop === ('accelerator' || 'brake')) {
          channel = 'state';
        } else {
          channel = 'alert';
        }
      }
    });
    state.onChange.handlers.forEach((handler: StateChangeHandler) => {
      handler(state, channel);
    });
  }
};
state.onChange.handlers      = [];
state.triggerChange.oldState = {};

// Handler for Socket
io.on('connection', (socket) => {
  // Handler function in state.triggerChange
  state.onChange((newState, channel) => {
    let mess = {
      state : () => {
        io.emit('state', {
        'accelerator' : newState.accelerator,
        'brake'       : newState.brake,
        })
      },
      alert : () => {
        io.emit('alert', {
          'drs'       : newState.drs,
          'rollBar'   : newState.rollBar,
          'glv'       : newState.glv
        })
      }
    };
    // Send the data
    mess[channel]();
  });

});

//---------------------------------------------------------------------------//
//-------------------------- MICROCONTROLLER SIDE ---------------------------//
//---------------------------------------------------------------------------//

const SerialPort = require('serialport'); // Serial port communication

require('console.table'); // Table for output data in the terminal

// Problem to be solved
const portname = process.argv[2];

var serial = new SerialPort(portname, {
  baudRate: 38400,
  parser: SerialPort.parsers.readline('\0')
});

// If open communicaiton
serial.on('open', () => {
  // It send start message of the board
  serial.write('start', (error) => {
    if (error) {
      console.log(color.err(`Error opening port: ${error.message}`));
      return;
    }

    console.log(color.log('The communication with board ...'));
  });

  console.log(color.ok('The communication with board open'));
});

// Receiving data
serial.on('data', (data: string) => {
  let parsingData: string[] = data.split(': '); // Parsing the message

  state[parsingData[0]] = Number(parsingData[1]);
  state.triggerChange();  // Upgrade the state and sending to the webserver

  // Output da
  console.table(color.log(parsingData));
});
